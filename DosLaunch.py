#
# DOSLaunch.py
# Version 1.0, September 2016
# Written by Adam Bauman (adam@kungfutreachery.net), https://bitbucket.org/adambauman/python_doslaunch)
#
# Creates custom configuration files for individual machines and programs, then executes DOSBox and any necessary
# support applications like Munt or VirtualMIDISyth.
#
# Sample configuration files are located in config_machines\ and config_programs\
#
# Copyright (C) 2016, Adam J. Bauman
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import ConfigParser
import os
import getopt
import sys
import socket
import subprocess
import psutil

working_directory = os.getcwd()
machine_name = socket.gethostname()
config_machine = ConfigParser.SafeConfigParser()
config_machine_diff = ConfigParser.SafeConfigParser()
config_program = ConfigParser.SafeConfigParser()

# Start the log file
log_file = open(working_directory+'\\doslaunch.log', 'w')
log_file.write('Begin script execution...\n')

#
# write_machine_diff(program_name=None)
# program_name: string, name of program config to set (matches cfg file in config_programs directory)
#
# Writes the machine-specific differential configuration. Machine configuration file should be located in the
# machine_configs\ directory and match the hostname of the host computer.
#
def write_machine_diff(program_name=None):
    config_machine_diff.add_section('sdl')
    config_machine_diff.add_section('midi')

    # Grab our machine configuration entries
    value_virtualmidisynthpath = config_machine.get('sound', 'virtualmidisynthpath')
    value_muntpath = config_machine.get('sound', 'muntpath')
    value_virtualmidisynthport = config_machine.get('sound', 'virtualmidisynthport')
    value_muntport = config_machine.get('sound', 'muntport')
    value_overridemidiport = config_machine.get('sound', 'overridemidiport')
    value_fullresolution = config_machine.get('video', 'fullresolution')
    value_windowresolution = config_machine.get('video', 'windowresolution')

    # Default values for Munt and VirtualMIDIDSynth, overwritten if program_name is set
    value_virtualmidisynthenabled = False
    value_muntenabled = False

    # Now read the Munt and VirtualMIDISynth settings from the program config (if present)
    if program_name:
        config_program.read('config_programs\\'+program_name+".cfg")
        value_virtualmidisynthenabled = config_program.get('doslaunch', 'usevirtualmidisynth')
        value_muntenabled = config_program.get('doslaunch', 'usemunt')

        # Lock in the enabled values so there's no room for weirdness later
        if value_virtualmidisynthenabled.lower() == 'true':
            value_virtualmidisynthenabled = True
        else:
            value_virtualmidisynthenabled = False

        if value_muntenabled.lower() == 'true':
            value_muntenabled = True
        else:
            value_muntenabled = False

    # If set make sure VirtualMIDISynth is running, port should be set in program configs
    if value_virtualmidisynthpath and value_virtualmidisynthport and value_virtualmidisynthenabled:
        config_machine_diff.set('midi', 'midiconfig', value_virtualmidisynthport)
        log_file.write('VirtualMIDISynth enabled on port '+value_virtualmidisynthport+'...\n')
        if "VirtualMIDISynth.exe" in [psutil.Process(i).name() for i in psutil.pids()]:
            log_file.write("VirtualMIDISynth.exe detected...\n")
        else:
            log_file.write("VirtualMIDISynth.exe not detected, attempting to start...\n")
            subprocess.Popen(value_virtualmidisynthpath+"\\virtualmidisynth.exe")

            # Keep banging on psutil.Process() until VirtualMIDISynth is loaded.
            virtualmidisynthready = False
            while virtualmidisynthready is False:
                if "VirtualMIDISynth.exe" in [psutil.Process(i).name() for i in psutil.pids()]:
                    print 'Waiting for VirtualMIDISynth to load...'
                    log_file.write('Waiting for VirtualMIDISynth to load...\n')
                    virtualmidisynthready = True

    # If set make sure Munt is running, port should be set in program configs
    if value_muntpath and value_muntport and value_muntenabled:
        config_machine_diff.set('midi', 'midiconfig', value_muntport)
        log_file.write('Munt enabled on port '+value_muntport+'...\n')
        if "mt32emu-qt.exe" in [psutil.Process(i).name() for i in psutil.pids()]:
            log_file.write("mt32emu-qt.exe detected...\n")
        else:
            log_file.write("mt32emu-qt.exe not detected, attempting to start...\n")
            try:
                subprocess.Popen(value_muntpath+"\\mt32emu-qt.exe")
            except:
                log_file.write('Failure to launch "'+value_muntpath+'\\mt32emu-qt.exe", exiting...\n')
                print 'Failure to launch "'+value_muntpath+'\\mt32emu-qt.exe", exiting...\n'
                sys.exit(2)

    # Check the rest, set the diff file as needed
    if value_overridemidiport:
        config_machine_diff.set('midi', 'midiconfig', value_overridemidiport)

    if value_fullresolution:
        config_machine_diff.set('sdl', 'fullresolution', value_fullresolution)

    if value_windowresolution:
        config_machine_diff.set('sdl', 'windowresolution', value_windowresolution)

    with open('machine_diff.cfg', 'wb') as config_file:
        config_machine_diff.write(config_file)

#
# launch_base()
#
# Launches DOSBox using only the base.cfg and machine differential configurations.
#
def launch_base():
    # Check that the DOSBox path is filled, can't continue without it
    machine_config_location = working_directory+'config_machines\\' + machine_name + ".cfg"
    config_machine.read(machine_config_location)

    if config_machine.get('dosbox', 'dosboxpath'):
        # Create the machine_diff.cfg file for additional launch options
        write_machine_diff()

        dosbox_path = config_machine.get('dosbox', 'dosboxpath') + '\\dosbox.exe'
        dosbox_mount_path = config_machine.get('dosbox', 'dosboxmountpath')

        dosbox_command = [dosbox_path,
                          '-noconsole',
                          '-conf', working_directory+'\\base.cfg',
                          '-conf', working_directory+'\\machine_diff.cfg',
                          '-c', 'mount C '+dosbox_mount_path
                          ]

        # Launch, if initial attempt fails message the path a little
        try:
            log_file.write("Attempting to run DOSBox with the following command:\n")
            for item in dosbox_command:
                log_file.write('\n'+item)

            log_file.write('\n')

            subprocess.Popen(dosbox_command)
        except:
            log_file.write('Failed to execute DOSBox at "'+dosbox_path+'"... Exiting\n')
            print 'Cannot execute DOSBox at "'+dosbox_path+'"\n' \
                  'Check your machine configuration file and try again. (Base Config)'
            sys.exit(2)

    else:
        print 'Loaded configuration for '+machine_name+' but it\'s incomplete.\n\n' \
              'Open the file in your text editor and make sure all paths are correct.\n'

        raw_input('Press any key to exit...')
        sys.exit(2)


#
# launch_program(program_name)
# program_name: string, name of program config to set (matches cfg file in config_programs directory)
#
# Launches DOSBox with the base.cfg, machine differential and program differential configurations. It should also
# launch the program once DOSBox loads as long as the path and executable are properly set in the program config file.
#
def launch_program(program_name):
    machine_config_location = working_directory+'\\config_machines\\' + machine_name + ".cfg"
    config_machine.read(machine_config_location)
    log_file.write('Reading machine config from "'+machine_config_location+'"\n')

    # Check that the DOSBox path is filled, can't continue without it
    if config_machine.get('dosbox', 'dosboxpath'):
        # Create the machine_diff.cfg file for additional launch options
        write_machine_diff(program_name)

        # Set the normal DOSBox.exe launch information
        dosbox_path = config_machine.get('dosbox', 'dosboxpath')
        dosbox_executable = "dosbox.exe"
        dosbox_base = "base.cfg"
        dosbox_mount_path = config_machine.get('dosbox', 'dosboxmountpath')

        # Read in the program configuration
        try:
            program_config_location = working_directory+'\\config_programs\\'+program_name+'.cfg'
            log_file.write('Reading program config from "'+program_config_location+'"\n')

            config_program.read(program_config_location)
            alternatedosboxexec = config_program.get('doslaunch', 'alternatedosboxexec')
            program_path = config_program.get('doslaunch', 'programpath')
            program_executable = config_program.get('doslaunch', 'programexecutable')
        except:
            log_file.write('!!! Program configuration error, exiting...\n')
            print 'Program configuration error. Make sure the config file "'+program_name+'.cfg"\:exception' \
                  'exists and all values are correct.'
            sys.exit(2)

        # If we're running an alternate DOSBox configuration we need to mix things up a little
        if alternatedosboxexec:
            try:
                alternate_dosbox_path = config_machine.get('dosboxalt'+alternatedosboxexec, 'alternatedosboxpath')
                alternate_dosbox_exec = config_machine.get('dosboxalt'+alternatedosboxexec, 'alternatedosboxexec')
                alternate_dosbox_base = config_machine.get('dosboxalt'+alternatedosboxexec, 'alternatedosboxbase')

                if alternate_dosbox_path and alternate_dosbox_exec:
                    log_file.write('Alternate DOSBox config is good, swapping in the values...\n')
                    dosbox_path = alternate_dosbox_path
                    dosbox_executable = alternate_dosbox_exec

                if alternate_dosbox_base:
                    log_file.write('Alternate DOSBox base is set to '+alternate_dosbox_base+'...\n')
                    dosbox_base = alternate_dosbox_base

            except:
                log_file.write('!!! Alternate DOSBox configuration error, exiting...\n')
                print 'Error loading alternate DOSBox configuration, exiting...\n'
                sys.exit(2)

        # Very similar to a base launch except we add the program's config to the chain (the machine_diff should
        # always be the last in the chain)
        dosbox_command = [dosbox_path+'\\'+dosbox_executable,
                          '-noconsole',
                          '-conf', working_directory+'\\'+dosbox_base,
                          '-conf', working_directory+'\\config_programs\\'+program_name+'.cfg',
                          '-conf', working_directory+'\\machine_diff.cfg',
                          '-c', 'mount C '+dosbox_mount_path,
                          '-c', 'c:',
                          '-c', 'cd '+program_path,
                          '-c', program_executable,
                          '-c', 'exit'
                          ]

        # Launch, if initial attempt fails message the path a little
        try:
            log_file.write("Attempting to run DOSBox with the following command:\n")
            for item in dosbox_command:
                log_file.write('\n'+item)

            log_file.write('\n')

            subprocess.Popen(dosbox_command, cwd=dosbox_path)
        except:
            print 'Cannot execute DOSBox at "'+dosbox_path+'", check your machine\n' \
                  'configuration file and try again.'
            sys.exit(2)

    else:
        print 'Loaded configuration for '+machine_name+' but it\'s incomplete.\n\n' \
              'Open the file in your text editor and make sure all paths are correct.\n'

        raw_input('Press any key to exit...')
        sys.exit(2)


#
# print_usage()
#
# Prints the command help if invalid or help arguments are passed.
#
def print_usage():
    print 'Launches DOSBox using custom per-machine configurations defined by the user.\n\n' \
          'Copyright (C) 2016, Adam J. Bauman\n' \
          'This program comes with ABSOLUTELY NO WARRANTY;\n' \
          'This is free software, and you are welcome to redistribute it' \
          'under certain conditions; See included license file for details.\n\n' \
          'DOSLaunch [-p program_name]\n\n' \
          '[-p program_name]\n' \
          '        Specifies the custom program config file to append, read from the \n' \
          '        config_programs directory. It does not support spaces or special\n' \
          '        characters in names.\n\n' \
          'If no arguments are presented DOSBox will be launched using only the base and\n' \
          'machine-specific configuration files. Machine files are choosen based on the\n' \
          'computer\'s host name.\n'


#
# main(argv)
# argv: command line arguments passed to the script.
#
# DOSLaunch.py's main execution function.
#
def main(argv):
    # Process command arguments
    try:
        opts, args = getopt.getopt(argv, "h:help:p:")
    except getopt.GetoptError:
        print_usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print_usage()
            sys.exit()
        elif opt in '-p':
            program_name = arg

    # If no arguments are passed, run with base + machine configuration
    if len(sys.argv) <= 1:
        machine_config_location = working_directory+'\\config_machines\\' + machine_name + ".cfg"
        if config_machine.read(machine_config_location):
            log_file.write('Running with base configuration at '+machine_config_location+"\n")
            launch_base()
        else:
            print 'Missing machine configuration for '+machine_name+'.\n\n' \
                  'If this computer hasn\'t been configured yet, run DOSLaunchSetup.exe\n'
            raw_input('Press any key to exit...')
            sys.exit(2)
    elif program_name:
        log_file.write('Running with program configuration for "'+program_name+'"\n')
        launch_program(program_name)
    else:
        log_file.write("Something went terribly wrong at the sys argument level... Exiting\n")

    log_file.write('\n\n')
    log_file.write('DOSLaunch execution complete, goodbye!')
    log_file.close()

#
# Goofy bit where it all begins
#
if __name__ == "__main__":
    main(sys.argv[1:])

