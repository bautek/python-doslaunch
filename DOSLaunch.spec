# -*- mode: python -*-

block_cipher = None



a = Analysis(['DOSLaunch.py'],
             pathex=['C:\\Users\\Adam\\PycharmProjects\\DosLaunch'],
             datas= [('base.cfg', '.'),
                     ('base-daum.cfg', '.'),
                     ('DOSLaunch Readme.txt', '.'),
                     ('License.txt', '.'),
                     ('config_machines\*.cfg', 'config_machines'),
                     ('config_programs\*.cfg', 'config_programs')
              ],
             binaries=None,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='DOSLaunch',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='DOSLaunch')
