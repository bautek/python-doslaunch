# README #

### BauTek DOSLaunch ###

* Launches DOSBox using machine and program specific differential configuration files.
* 1.0 (September 2016)

### DOSLaunch Structure ###

Launch configuration chain:  base.cfg -> program_specific.cfg (optional) -> machine_specific.cfg

DOSLaunch.py:  the launching script, use with -p <programname> switch to specify program configurations
base.cfg:  the base DOSBox configuration file, applied to ALL executions
config_machines\:  location where machine configurations reside. Files name for the hostname of the computer
config_programs\:  location where program configurations reside. Call them whatever you want as long as it makes sense to you
DOSLaunch_x_x.zip:  a distributable version of DOSLaunch that's all bundled up and ready to go. Good for people not wanting to play with the source

### How do I get set up? ###

* Install DOSBox, create a mount folder for your DOSBox VM
* (optional) Install and configure VirtualMIDISynth (how, why? http://kungfutreachery.net/quest-better-dosbox-music/)
* (optional) Install and configure Munt (how, why? http://kungfutreachery.net/quest-better-dosbox-music/)
* Create a new machine configuration in config_machines using your computer's hostname and ".cfg" as the filename (can be found by typing "hostname" or "ipconfig" in a command prompt)
* Modify base.cfg to your liking
* Create program-specific configurations
* Execute DOSLaunch on its own with the Python file or the EXE in the dist\DOSBox folder to start DOSBox with your machine and base configs loaded
* Execute DOSLaunch with the "-p" switch and a program configuration name to execute DOSBox with your machine, base and a program configuration file

### FAQ ###

* What do I need this for?  If you like to play the same games across multiple machines this makes managing MIDI mappers and other options easier.
* If I'm using multiple machines how do I keep all my saves and stuff synced?  I dump the games I'm currently playing on OneDrive and let the service sort it all out.
* Can I use this to launch from Steam or other front-ends? Sure can, I use it with Steam and Hyper-Spin. The executable in the dist\DOSLaunch folder works best when using with launchers usually easier overall if you just want to use the program)
* Why bother with VirtualMIDISynth and/or Munt?  Checkout the article and video posted above, both can make your games sounds MUCH better.
* Where do I find Munt ROMs?  Search Google for "mt32 roms" and see of any Vogons results pop up. The versions I've been using for awhile are "CM32L_CONTROL.1989-12.05.v1.02.ROM" and "CM32L_PCM.ROM"  You will need the matching PCM and CONTROL ROMs for Munt to function.
* Something went wrong and DOSBox won't launch, what do I do?  Keep calm and check the .log files in the DOSLaunch working directory. They should let you know what's wrong and help you work towards a solution.