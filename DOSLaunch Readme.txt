================
 DOSLaunch v1.0
================

A simple launcher for a unified DOSBox experience on multiple machines
(https://bitbucket.org/adambauman/python_doslaunch)

Copyright (C) 2016, Adam J. Bauman
 This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


Installation:
-------------
1. Install DOSBox, Munt (optional) and VirtualMIDISynth (also optional)
2. Copy the contents of dist\DOSLaunch to a location of your choosing
3. Copy the config_machines\example.cfg file to config_machines\hostname_of_your_computer.cfg and modify its 
	values to match your machine (use the "hostname" or "ipconfig" commands to find your computer's name)
4. Modify your base.cfg file to setup DOSBox to your liking
5. Copy the config_programs\example.cfg file to config_programs\name_of_program.cfg and modify the values for 
	your program(s)
6. Run DOSLaunch with or without options and have fun!


Usage:
------
DOSLaunch [-p program_name]

	[-p program_name]
      		Specifies the custom program config file to append, read from the
		config_programs directory. It does not support spaces or special
		characters in names.

	If no arguments are presented DOSBox will be launched using only the base and
	machine-specific configuration files. Machine files are choosen based on the
	computer's host name.


Configuration Chain:
--------------------

Configurations are always loaded in the following order. Values set in files early in the chain will be overwritten
by values defined further on.

.\base.cfg -> config_programs\program_name.cfg (if defined) -> config_machines\machine.cfg


Troubleshooting:
----------------

If DOSLaunch fails to launch correctly check the DOSLaunch.log file located in the program directory. If that fails
to turn up any clues, try running DOSLaunch with or without options from a command prompt, it should output
additional error messages or help you narrow down configuration file problems. If it's a configuration problem
modify your config files, paying close attention to the way paths are presented and the comments.